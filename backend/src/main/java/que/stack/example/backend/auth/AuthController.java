package que.stack.example.backend.auth;

import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.security.AuthenticationFailedException;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.NoSuchElementException;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
public class AuthController {

    @Inject
    UserRepository userRepository;

    @Inject
    TokenGenerator tokenGenerator;

    @GET
    @PermitAll
    public String getAuthStatus(@Context SecurityContext ctx) {
        return getResponseString(ctx);
    }

    private String getResponseString(SecurityContext ctx) {
        String name;
        if (ctx.getUserPrincipal() == null) {
            name = "anonymous";
        } else {
            name = ctx.getUserPrincipal().getName();
        }
        return String.format("hello + %s,"
                        + " isHttps: %s,"
                        + " authScheme: %s",
                name, ctx.isSecure(), ctx.getAuthenticationScheme());
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response login (UserDTO userDTO) {
        User user;

        if (userDTO.getPassword() == null)
            return Response.status(401).build();

        try {
            user = userRepository.getByUsername(userDTO.getUsername());
            if (user == null) throw new NoSuchElementException();
        } catch (NoSuchElementException | AuthenticationFailedException e) {
            return Response.status(401).build();
        }

        if (!BcryptUtil.matches(userDTO.getPassword(), user.getPassword()))
            return Response.status(401).build();

        String authToken = tokenGenerator.generateAdminToken(user.getUsername());
        return Response.ok(authToken).build();
    }

    @GET
    @Path("/users")
    @RolesAllowed("ADMIN")
    public List<User> allUsers () {
        return userRepository.getAll();
    }
}
