package que.stack.example.backend.auth;

import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class TokenGenerator {

    @ConfigProperty(name = "mp.jwt.verify.issuer")
    String issuer;

    /**
     * Generate JWT token
     */
    public String generateAdminToken(String username) {
        return Jwt.issuer(issuer)
                .upn(username)
                .groups("ADMIN").sign();
    }
}
