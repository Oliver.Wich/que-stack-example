package que.stack.example.backend;

import que.stack.example.backend.auth.UserRepository;
import io.quarkus.runtime.StartupEvent;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class Startup {

    private static final Logger logger = Logger.getLogger(Startup.class);

    @Inject
    UserRepository userRepository;

    @ConfigProperty(name = "application.admin.username")
    String adminUsername;

    @ConfigProperty(name = "application.admin.password")
    String adminPassword;

    public void loadAdminUser(@Observes StartupEvent evt) {
        userRepository.deleteByUsername(adminUsername);
        userRepository.create(adminUsername, adminPassword);
    }
}
