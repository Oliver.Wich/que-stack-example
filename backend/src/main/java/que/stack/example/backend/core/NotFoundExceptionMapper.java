package que.stack.example.backend.core;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Scanner;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * NotFoundExepptionMapper
 */
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    /**
     * Redirects every unhandled URL to the SPA.
     * If the index.html file of the spa is not found (e.g. during build) it will display a 404 page.
     *
     * @return Response
     */
    @Override
    public Response toResponse(NotFoundException exception) {
        Response.Status status = Response.Status.SEE_OTHER;
        InputStream htmlFile = this.getClass().getResourceAsStream("/META-INF/resources/index.html");

        if (htmlFile == null) {
            htmlFile = this.getClass().getResourceAsStream("/public/static/404.html");
            status = Response.Status.NOT_FOUND;
        }

        String text = new Scanner(Objects.requireNonNull(htmlFile), StandardCharsets.UTF_8).useDelimiter("\\A").next();
        return Response.status(status).entity(text).build();
    }
}
