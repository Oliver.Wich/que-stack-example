package que.stack.example.backend;

import que.stack.example.backend.auth.UserDTO;
import que.stack.example.backend.auth.UserRepository;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class AuthControllerTest {

    @Inject
    UserRepository userRepository;

    static final String testUsername = "TestUser" + RandomStringUtils.randomAlphabetic(5);
    static final String testPassword = "TestPassword" + RandomStringUtils.randomAlphabetic(16);

    @BeforeEach
    void createTestUser() {
        userRepository.create(testUsername, testPassword);
    }

    @AfterEach
    void deleteTestUser() {
        userRepository.deleteByUsername(testUsername);
    }

    @Test
    public void testAuthEndpointAnonymous() {
        Response response = given()
                .when().get("/auth")
                .then()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals("hello + anonymous, isHttps: false, authScheme: null", response.asString());
    }

    @Test
    public void testUserEndpointAnonymous() {
        Response response = given()
                .when().get("/auth/users")
                .then()
                .extract().response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    @TestSecurity(user = "TestUser", roles = "OTHER")
    public void testUserEndpointOther() {
        Response response = given()
                .when().get("/auth/users")
                .then()
                .extract().response();

        Assertions.assertEquals(403, response.statusCode());
    }

    @Test
    @TestSecurity(user = "TestUser", roles = "ADMIN")
    public void testUserEndpointAdmin() {
        Response response = given()
                .when().get("/auth/users")
                .then()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
    }


    @Test
    public void testWrongPasswordLogin() {
        UserDTO user = new UserDTO(testUsername, "wrong");

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(user)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void testMissingPasswordLogin() {
        UserDTO user = new UserDTO();
        user.setUsername(testUsername);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(user)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void testMissingUsernameLogin() {
        UserDTO user = new UserDTO();
        user.setPassword(testPassword);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(user)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void testAdminUserLogin() {
        UserDTO user = new UserDTO(testUsername, testPassword);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(user)
                .when().post("/auth/login")
                .then()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertFalse(response.asString().isEmpty());

        // Test if token works
        Response usersResponse = getAuthUsersEndpointWithToken(response.asString());
        Assertions.assertEquals(200, usersResponse.statusCode());
    }

    private Response getAuthUsersEndpointWithToken (String token) {
        return given()
                .header("Authorization", "Bearer " + token)
                .when().get("/auth/users")
                .then()
                .extract().response();
    }

}
