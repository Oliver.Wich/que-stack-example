# QUE-Stack example

The "QUE" stack consists of 2 modules, [backend] and [frontend].
[backend] is build using the Java framework [quarkus](https://quarkus.io).
[frontend] is using Vue 3 with typescript to build a spa. The files will be build into the quarkus static resources
folder and served for every url where the backend has no native endpoint for. So if you handle e.g. the `/api` endpoint
in quarkus, it will always answer first. On all other routes that are *not* defined in quarkus, the request will be
forwarded to the frontend and handled there.
Maven is used as a bundler to combine those 2 modules into one project.

## Setup
Generate a private and public key in `backend/src/main/resources`:
```shell
openssl genrsa -out rsaPrivateKey.pem 2048
openssl rsa -pubout -in rsaPrivateKey.pem -out publicKey.pem

openssl pkcs8 -topk8 -nocrypt -inform pem -in rsaPrivateKey.pem -outform pem -out privateKey.pem
```

## Installation

1. Import `que-stack-example` Maven Package
2. Let Intellij figure out everything and install the maven packages
3. For the frontend you will need to run `npm install` separately
4. Choose `RunAll` run configuration. This will start Quarkus and the Vue frontend

- frontend will be available on [http://localhost:8080/](http://localhost:8080/)
- All other routes will be passed to Quarkus

### Database Inspection

To setup an IntelliJ database connection:

1. Select H2 as Datasource
2. modify the connection URL to look like this:
   `jdbc:h2:./backend/PATH/TO/DB-FILE>;AUTO_SERVER=TRUE`
   This should be the same value set for `quarkus.datasource.jdbc.url` in application.properties 
   but the path starts with `./backend/` instead of just './'
3. Connect + Refresh
##
